package ru.rsreu.oop.paint.paintcore.service;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rsreu.oop.paint.paintcore.domain.Figure;
import ru.rsreu.oop.paint.paintcore.domain.TypeFigure;
import ru.rsreu.oop.paint.paintcore.exception.NotFoundFigure;
import ru.rsreu.oop.paint.paintcore.utils.MessageAlert;

import java.util.List;

@Component
public class ControllerUserInterface {

    @FXML
    public ToggleGroup but;
    @FXML
    private Spinner size;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private Canvas canvas;
    @FXML
    private Menu menu;

    private String selectedIndex;

    private TypeFigure typeFigure;

    @Autowired
    private PaintService paintService;
    @Autowired
    private List<Figure> figures;

    public void loadSpinner() {
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 15);
        size.setValueFactory(valueFactory);
        menu.setOnAction(e -> selectedIndex = ((MenuItem) e.getTarget()).getText());
    }

    public void paintFigure(MouseEvent mouseEvent) {
        if (typeFigure!=null) {
            Figure figure = figures.parallelStream().filter(figure1 -> figure1.getTypeFigure().equals(typeFigure)).findFirst().orElseThrow(NotFoundFigure::new);
            figure.init(mouseEvent.getX(), mouseEvent.getY(), Double.valueOf(size.getValue().toString()), colorPicker.getValue());
            paintService.draw(figure, canvas);
        } else {
            MessageAlert.showMessage(Alert.AlertType.INFORMATION, "Выберете инструмент для рисования");
        }
    }

    private void selectFigure(String selectedIndex) {
        switch (selectedIndex) {
            case "Круг":
                typeFigure = TypeFigure.CIRCLE;
                break;
            case "Квадрат":
                typeFigure = TypeFigure.SQUARE;
                break;
            default:
                MessageAlert.showMessage(Alert.AlertType.ERROR, "Неизвестный инструмент!");
                break;
        }
    }

    public void selectFigureByMenu() {
        selectFigure(selectedIndex);
    }

    public void selectFigureByButton() {
        selectFigure(((ToggleButton) but.getSelectedToggle()).getText());
    }

    public void exit() {
        if (MessageAlert.showMessage(Alert.AlertType.CONFIRMATION, "Вы действительно хотите выйти?").getResult() == ButtonType.YES) {
            System.exit(0);
        }
    }

    public void clear() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

}
