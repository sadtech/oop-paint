package ru.rsreu.oop.paint.paintcore;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Lazy;
import ru.rsreu.oop.paint.paintcore.config.ConfigurationControllers;
import ru.rsreu.oop.paint.paintcore.service.ControllerUserInterface;

@Lazy
@SpringBootApplication
public class PaintCoreApplication extends AbstractJavaFxApplicationSupport {

    @Value("${ui.title:Графический редактор}")//
    private String windowTitle;

    @Autowired
    private ConfigurationControllers.View view;

    @Override
    public void start(Stage stage) {
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(view.getParent()));
        stage.setOnShown(e -> ((ControllerUserInterface) view.getController()).loadSpinner());
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
    }

    public static void main(String[] args) {
        launchApp(PaintCoreApplication.class, args);
    }


}
