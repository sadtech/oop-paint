package ru.rsreu.oop.paint.paintcore.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ru.rsreu.oop.paint.paintcore.PaintCoreApplication;

import java.util.Objects;

public class MessageAlert {

    private MessageAlert() {
        throw new IllegalStateException("Utility class");
    }

    public static Alert showMessage(AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setHeaderText(null);
        alert.setTitle(title(alertType));
        alert.setContentText(message);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(Objects.requireNonNull(PaintCoreApplication.class.getClassLoader()
                .getResource(urlImage(alertType))).toString()));
        alert.showAndWait();
        return alert;
    }

    private static String urlImage(AlertType alertType) {
        switch (alertType) {
            case INFORMATION:
                return "image/message.png";
            case ERROR:
                return "image/error.png";
            case CONFIRMATION:
                return "image/confirmation.png";
            default:
                return null;
        }
    }

    private static String title(AlertType alertType) {
        switch (alertType) {
            case ERROR:
                return "Error";
            case INFORMATION:
                return "Information";
            default:
                return null;
        }
    }


}
