package ru.rsreu.oop.paint.paintcore.domain;

import javafx.scene.paint.Color;

public abstract class Figure {

    private double x;
    private double y;
    private double size;
    private Color color;
    private TypeFigure typeFigure;

    public Figure() {

    }

    public void init(double x, double y, double size, Color color) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public TypeFigure getTypeFigure() {
        return this.typeFigure;
    }

    public void setTypeFigure(TypeFigure typeFigure) {
        this.typeFigure = typeFigure;
    }
}
