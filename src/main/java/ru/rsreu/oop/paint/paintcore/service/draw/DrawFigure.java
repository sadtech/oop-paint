package ru.rsreu.oop.paint.paintcore.service.draw;

import javafx.scene.canvas.GraphicsContext;
import ru.rsreu.oop.paint.paintcore.domain.Figure;

public interface DrawFigure<F extends Figure> {

    void draw(GraphicsContext graphicsContext, F figure);

}
