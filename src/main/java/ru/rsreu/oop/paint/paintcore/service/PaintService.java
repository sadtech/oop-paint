package ru.rsreu.oop.paint.paintcore.service;

import javafx.scene.canvas.Canvas;
import ru.rsreu.oop.paint.paintcore.domain.Figure;

@FunctionalInterface
public interface PaintService<F extends Figure> {
    void draw(F figure, Canvas canvas);
}
