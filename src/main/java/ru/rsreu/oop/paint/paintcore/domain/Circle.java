package ru.rsreu.oop.paint.paintcore.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Circle extends Figure {

    public Circle() {
        super.setTypeFigure(TypeFigure.CIRCLE);
    }
}
