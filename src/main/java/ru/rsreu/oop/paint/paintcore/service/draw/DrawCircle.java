package ru.rsreu.oop.paint.paintcore.service.draw;

import javafx.scene.canvas.GraphicsContext;
import ru.rsreu.oop.paint.paintcore.domain.Circle;

public class DrawCircle implements DrawFigure<Circle> {
    @Override
    public void draw(GraphicsContext graphicsContext, Circle figure) {
        graphicsContext.strokeOval(figure.getX(), figure.getY(), figure.getSize(), figure.getSize());
    }
}
