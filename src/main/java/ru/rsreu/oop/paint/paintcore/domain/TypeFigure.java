package ru.rsreu.oop.paint.paintcore.domain;

public enum TypeFigure {

    CIRCLE, SQUARE

}
