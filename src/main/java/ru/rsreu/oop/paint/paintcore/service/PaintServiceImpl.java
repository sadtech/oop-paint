package ru.rsreu.oop.paint.paintcore.service;

import com.sun.istack.internal.NotNull;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.rsreu.oop.paint.paintcore.domain.Figure;
import ru.rsreu.oop.paint.paintcore.domain.TypeFigure;
import ru.rsreu.oop.paint.paintcore.exception.NotFoundFigure;
import ru.rsreu.oop.paint.paintcore.service.draw.DrawCircle;
import ru.rsreu.oop.paint.paintcore.service.draw.DrawFigure;
import ru.rsreu.oop.paint.paintcore.service.draw.DrawSquare;

import java.util.Optional;

@Service
public class PaintServiceImpl implements PaintService {

    private static final Logger log = LoggerFactory.getLogger(PaintServiceImpl.class);

    @Override
    public void draw(@NotNull Figure figure, Canvas canvas) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(figure.getColor());
        DrawFigure drawFigure = Optional.ofNullable(drawFigure(figure.getTypeFigure())).orElseThrow(NotFoundFigure::new);
        drawFigure.draw(graphicsContext, figure);
    }

    private DrawFigure drawFigure(TypeFigure typeFigure) {
        switch (typeFigure) {
            case CIRCLE:
                return new DrawCircle();
            case SQUARE:
                return new DrawSquare();
            default:
                log.info("Такой фигуры не существует");
                throw new NotFoundFigure();
        }
    }


}
