package ru.rsreu.oop.paint.paintcore.service.draw;

import javafx.scene.canvas.GraphicsContext;
import ru.rsreu.oop.paint.paintcore.domain.Square;

public class DrawSquare implements DrawFigure<Square> {
    @Override
    public void draw(GraphicsContext graphicsContext, Square figure) {
        graphicsContext.strokeRect(figure.getX(), figure.getY(), figure.getSize(), figure.getSize());
    }
}
