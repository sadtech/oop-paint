package ru.rsreu.oop.paint.paintcore.exception;

public class NotFoundFigure extends RuntimeException {

    public NotFoundFigure(String message) {
        super(message);
    }

    public NotFoundFigure() {

    }
}
